<?php

$str_logFile = "logfile.txt";

$filehandler = fopen($str_logFile, 'a') or die('failure');

$now = new DateTime();
$str_now = $now->format('d.m.Y H:i:s');

$arr_content = array(
	$str_now,
	$_POST['action'],
	$_POST['url'],
	$_POST['message']
);

$str_logContent = implode("\t", $arr_content) . "\n";

fwrite($filehandler, $str_logContent);

fclose($filehandler);