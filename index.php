<html>
    <head>
        <title>Magic Mirror</title>
        <link rel="stylesheet" type="text/css" href="styles/stylesheets/frontend.magicmirror.css"/>
        <script type="text/javascript">
			var gitHash = '<?php echo trim(`git rev-parse HEAD`) ?>';
        </script>

        <meta name="google" content="notranslate"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    </head>
    <body>

        <div>

            <div class="top left">
                <div id="timeContainer" style="display: none;">
                    <div class="date small dimmed"></div>
                    <div class="time"></div>
                </div>
                <div id="calendarContainer" class="calendar xxsmall" style="display: none;"></div>
            </div>


            <div class="top right">
                <div id="currentWeatherContainer" style="display: none;">
                    <div class="windsun small dimmed"></div>
                    <div class="temp"></div>
                </div>
                <div id="weatherForecastContainer" class="forecast small dimmed" style="display: none;"></div>
            </div>


            <div id="slideShowContainer" data-imagePosition="0" style="display: none;">
                <img/>
            </div>


            <div class="lower-third center-hor">
                <div class="compliment light"></div>
            </div>


            <div class="bottom center-hor">
                <div class="news medium"></div>
            </div>

        </div>

        <script src="js/jquery-2.1.4.js"></script>
        <script src="js/ical_parser.js"></script>
        <script src="js/rrule.js"></script>
        <script src="js/moment-with-langs.js"></script>
        <script src="js/config.js"></script>
        <script src="js/main.js?nocache=<?php echo md5(microtime()) ?>"></script>

    </body>
</html>
