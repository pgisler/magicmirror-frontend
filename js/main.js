jQuery.fn.updateWithText = function(text, speed) {
	var dummy = $('<div/>').html(text);

	if ($(this).html() != dummy.html())
	{
		$(this).fadeOut(speed/2, function() {
			$(this).html(text);
			$(this).fadeIn(speed/2, function() {
				//done
			});
		});
	}
};

jQuery.fn.outerHTML = function(s) {
    return s
        ? this.before(s).remove()
        : jQuery("<p>").append(this.eq(0).clone()).html();
};

function roundVal(temp) {
	return Math.round(temp * 10) / 10;
}

function checkGitPageReload() {
	if (power == 'on') {
		$.getJSON('githash.php', {}, function(json, textStatus) {
			if (json) {
				if (json.gitHash != gitHash) {
					window.location.reload();
					window.location.href=window.location.href;
				}
			}
		});
	}

	setTimeout(function() {
		checkGitPageReload();
	}, reloadInterval.pageReload);
}

function updateTime() {
	if (power == 'on' && widgetVisibility.time) {
		var now = moment();
		var date = now.format('LLLL').split(' ',4);
		date = date[0] + ' ' + date[1] + ' ' + date[2] + ' ' + date[3];

		$('.date').html(date);
		$('.time').html(now.format('HH') + ':' + now.format('mm') + '<span class="sec">'+now.format('ss')+'</span>');
	}

	setTimeout(function() {
		updateTime();
	}, reloadInterval.time);
}

function updateCalendarData() {

	if (power == 'on' && widgetVisibility.calendar) {
		// Die Daten vom Server laden
		$.getJSON(baseUrl + calendarUrlSuffix, {mirror: mirrorId}, function (calendarData) {
			// Die Eventliste leeren bevor neue Events eingetragen werden
			eventList = [];

			if (calendarData !== null && !calendarData.hasOwnProperty('error_message')) {
				// Jeden Event im empfangenen Resultat zur Eventliste hinzufügen
				for (var e in calendarData) {
					var event = calendarData[e];
					var now = moment();
					var startDate = moment(event.TIMESTAMP * 1000);
					var seconds = startDate.diff(now, 'seconds');
					var timeString;

					if (seconds <= 60 * 60 * 5 || seconds >= 60 * 60 * 24 * 2) {
						timeString = moment(startDate).fromNow();
					} else {
						timeString = moment(startDate).calendar()
					}

					eventList.push({
						'description': event.SUMMARY,
						'seconds': seconds,
						'days': timeString
					});
				}

				updateCalendar();
			} else if (calendarData.hasOwnProperty('error_message')) {
				logToFile('updateCalendarData', baseUrl + calendarUrlSuffix, 'ERROR: ' + calendarData['error_message']);
			}
		});
	}

	setTimeout(function() {
		updateCalendarData();
	}, reloadInterval.calendarData);
}

function updateWeatherData() {
	if (power == 'on' && (widgetVisibility.currentWeather || widgetVisibility.weatherForecast)) {
		// Die aktuellen Wetterdaten vom Server laden
		$.getJSON(baseUrl + weatherUrlSuffix, {mirror: mirrorId}, function (weatherData) {

			if (!weatherData.hasOwnProperty('error_message')) {

				/**
				 * Aktuelles Wetter
				 */
				if (widgetVisibility.currentWeather) {
					// Aktuelle Temperatur und Wetter-Icon
					var currentTemperatur = roundVal(weatherData.current.temperature);
					var currentIcon = $('<span/>').addClass('icon dimmed wi').addClass(iconTable[weatherData.current.icon]);

					// Aktuelle Windgeschwindigkeit und Sonnenaufgang/Sonnenuntergang
					var windSpeed = roundVal(weatherData.current.windSpeed);
					var windString = '<span class="wi wi-strong-wind xdimmed"></span> ' + windSpeed;

					var now = new Date(),
						sunrise = new Date(weatherData.current.sunrise * 1000),
						sunset = new Date(weatherData.current.sunset * 1000),
						sunString;

					if (sunrise < now && sunset >= now) {
						sunString = '<span class="wi wi-sunset xdimmed"></span> ' + sunset.toTimeString().substring(0, 5);
					} else {
						sunString = '<span class="wi wi-sunrise xdimmed"></span> ' + sunrise.toTimeString().substring(0, 5);
					}

					// Daten im Interface aktualisieren
					$('.temp').updateWithText(currentIcon.outerHTML() + currentTemperatur + '&deg;', 1000);
					$('.windsun').updateWithText(windString + ' ' + sunString, 1000);
				}

				/**
				 * Wettervorhersage
				 */
				if (widgetVisibility.weatherForecast) {
					var forecastTable = $('<table />').addClass('forecast-table');
					var opacity = 1;
					for (var i in weatherData.forecast) {
						var forecast = weatherData.forecast[i];
						var iconClass = iconTable[forecast.icon];
						var date = new Date(forecast.date * 1000);
						var row = $('<tr />').css('opacity', opacity);

						row.append($('<td/>').addClass('day').html(moment.weekdaysShort(date.getDay())));
						row.append($('<td/>').addClass('icon-small').addClass(iconClass));
						row.append($('<td/>').addClass('temp-max').html(roundVal(forecast.temperature.day) + "&deg;"));
						//row.append($('<td/>').addClass('temp-min').html(roundVal(forecast.temperature.night)));
						row.append($('<td/>').addClass('temp-min').html(roundVal(forecast.precipitation.value) + 'mm'));

						forecastTable.append(row);
						opacity -= 0.155;
					}

					$('.forecast').updateWithText(forecastTable, 1000);
				}
			} else {
				logToFile('updateWeatherData', baseUrl + weatherUrlSuffix, 'ERROR: ' + weatherData['error_message']);
			}

		});
	}

	setTimeout(function() {
		updateWeatherData()
	}, reloadInterval.weatherData);

}

function updateSlideShow() {

	console.log('update slideShow', power, widgetVisibility.slideShow);

	if (power == 'on' && widgetVisibility.slideShow) {
		// Die Position des aktuellen Bildes innerhalb der aktiven Galerie auslesen
		var imageContainer = $('#slideShowContainer');
		var imagePosition = parseInt($(imageContainer[0]).attr('data-imagePosition'));

		// Das nächste Bild vom Server laden
		jQuery.ajax({
			url: baseUrl + imageUrlSuffix,
			method: 'GET',
			data: {
				mirror: mirrorId,
				position: imagePosition + 1
			},
			dataType: 'json',
			jsonp: false,
			success: function(imageData) {
				if (!imageData.hasOwnProperty('error_message')) {
					// Das neue Bild-Objekt erstellen und als HTML zur Seite hinzufügen
					var srcString = 'data:' + imageData.mediaType + ';base64,' + imageData.img;
					var img = $('<img />').attr('src', srcString).attr('style', 'display: none;');
					imageContainer.append(img);
					imageContainer.attr('data-imagePosition', imageData.sortOrder);

					// Danach das alte Bild ausblenden
					var oldImage = imageContainer.children('img:first-child');
					oldImage.fadeOut({
						duration: 1000,
						complete: function () {
							// Nach dem Ausblenden kann das alte Bild entfernt werden
							oldImage.remove();

							// Schliesslich soll das neue Bild eingeblendet werden
							img.fadeIn({
								duration: 700,
								complete: function () {
									setTimeout(function () {
										updateSlideShow()
									}, imageData.updateInterval);
								}
							});
						}
					});
					logToFile('updateSlideShow', baseUrl + imageUrlSuffix, 'SUCCESS: New image loaded (pos: ' + imageData.sortOrder + ')');
				} else {
					console.error('Falsches Antwortformat.');
					updateSlideShow();
					logToFile('updateSlideShow', baseUrl + imageUrlSuffix, 'ERROR: ' + imageData['error_message'] + " [Parameters: {position: " + (imagePosition + 1) + "}]");
				}
			},
			error: function() {
				console.error('Fehler beim laden eines Bildes.');
				logToFile('updateSlideShow', baseUrl + imageUrlSuffix, 'ERROR: ' + 'Fehler beim Laden eines Bildes.' + " [Parameters: {position: " + (imagePosition + 1) + "}]");
				updateSlideShow();
			}
		});
	}
}

function updateWidgetVisibility() {
	if (power == 'on') {
		// Die aktuellen Daten vom Server laden
		$.getJSON(baseUrl + widgetVisibilityUrl, {mirror: mirrorId}, function (widgetVisibilityResponse) {

			for (var i in widgetVisibilityResponse) {

				if (widgetVisibility[i] != widgetVisibilityResponse[i]) {
					var container = jQuery('#' + i + 'Container');

					widgetVisibility[i] = widgetVisibilityResponse[i];

					if (widgetVisibilityResponse[i]) {

						switch (i) {
							case 'slideShow':
								updateSlideShow();
								break;
							case 'time':
								updateTime();
								break;
							case 'calendar':
								updateCalendarData();
								break;
							case 'currentWeather':
							case 'weatherForecast':
								updateWeatherData();
								break;
						}

						container.show();
					} else {
						container.hide();
					}
				}
			}

			var imageContainer = $('#slideShowContainer');
			if (widgetVisibility.slideShow && !widgetVisibility.calendar && !widgetVisibility.currentWeather && !widgetVisibility.weatherForecast) {
				imageContainer.css('padding-top', 0);
			} else {
				imageContainer.css('padding-top', '');
			}
		});
	}

	setTimeout(function() {
		updateWidgetVisibility();
	}, reloadInterval.widgetVisibility);
}

function updatePowerStatus() {
	// Die aktuellen Daten vom Server laden
	$.getJSON(baseUrl + powerEventUrlSuffix, {mirror: mirrorId}, function(response) {

		if (!response.hasOwnProperty('error_message') &&
			(response.hasOwnProperty('powerEvents') || response.hasOwnProperty('staticPowerStatus'))
		) {
			if (response.hasOwnProperty('staticPowerStatus')) {
				activatePowerStatus(response.staticPowerStatus);
			} else {
				if (response.hasOwnProperty('powerEvents') && response.powerEvents.hasOwnProperty('state')) {
					activatePowerStatus(response.powerEvents.state);
				}
			}
		}

	});

	setTimeout(function() {
		updatePowerStatus();
	}, reloadInterval.powerStatus);
}

var eventList = [];

function updateCalendar() {

	table = $('<table/>').addClass('xsmall').addClass('calendar-table');
	opacity = 1;

	for (var i in eventList) {
		var e = eventList[i];

		var row = $('<tr/>').css('opacity',opacity);
		row.append($('<td/>').html(e.description).addClass('description'));
		row.append($('<td/>').html(e.days).addClass('days dimmed'));
		table.append(row);

		opacity -= 1 / eventList.length;
	}

	$('.calendar').updateWithText(table,1000);
}

function activatePowerStatus(powerState) {
	if (powerState !== power) {
		$.getJSON(powercontrolUrl, {state: powerState}, function(response) {
			power = response.stateRequested;

			if (power == 'on') {
				updateSlideShow();
			}
		});
	}
}

function logToFile(action, url, message) {

	jQuery.ajax({
		url: loggerUrl,
		method: 'POST',
		data: {
			action: action,
			url: url,
			message: message
		},
		success: function(response) {
			console.log(response);
		}
	});

}



jQuery(document).ready(function($) {

    moment.lang(lang);

	checkGitPageReload();

	updateTime();

	updateCalendarData();

	updateWeatherData();

	updatePowerStatus();

	updateWidgetVisibility();

	updateSlideShow();

});
