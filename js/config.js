// Mirror ID
const mirrorId = '8be5ee35-b544-4886-8da1-783dd821b27c';		// "echter" Spiegel von Lavinia
//const mirrorId = '11654f89-6bc4-4a0b-afa2-a5abc45f637e';	// "debug" Spiegel in lokaler Entwicklungs-Umgebung

// current power status
var power = 'on';

// current widget visibility
var widgetVisibility = {
	time: false,
	calendar: false,
	currentWeather: false,
	weatherForecast: false,
	slideShow: false
};

// Remote URLs
const baseUrl = 'https://magicmirror.ch/';
//const baseUrl = 'http://backend.magicmirror.local/';
var calendarUrlSuffix = 'rest/calendar';
var weatherUrlSuffix = 'rest/weather';
var imageUrlSuffix = 'rest/image';
var powerEventUrlSuffix = 'rest/power';
var widgetVisibilityUrl = 'rest/widgetVisibility';

// Reload intervals
var reloadInterval = {
	pageReload: 60000,
	time: 1000,
	calendarData: 30000,
	weatherData: 60000,
	powerStatus: 60000,
	widgetVisibility: 60000
};

// Local URLs
var powercontrolUrl = 'powercontrol/powercontrol.php';
var loggerUrl = 'logs/logger.php';

// for navigator language
var lang = window.navigator.language;

// Weather icon mapping
var iconTable = {
	'01d': 'wi-day-sunny',
	'02d': 'wi-day-cloudy',
	'03d': 'wi-cloudy',
	'04d': 'wi-cloudy-windy',
	'09d': 'wi-showers',
	'10d': 'wi-rain',
	'11d': 'wi-thunderstorm',
	'13d': 'wi-snow',
	'50d': 'wi-fog',
	'01n': 'wi-night-clear',
	'02n': 'wi-night-cloudy',
	'03n': 'wi-night-cloudy',
	'04n': 'wi-night-cloudy',
	'09n': 'wi-night-showers',
	'10n': 'wi-night-rain',
	'11n': 'wi-night-thunderstorm',
	'13n': 'wi-night-snow',
	'50n': 'wi-night-alt-cloudy-windy'
};